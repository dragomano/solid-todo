import TodoItem from './TodoItem';
import TodoForm from './TodoForm';
import { onMount, createMemo, For } from "solid-js";
import { createStore } from "solid-js/store";

function TodoList(props) {
  const [store, setStore] = createStore({ todos: [] });

  onMount(() => {
    fetch('https://dummyapi.online/api/todos')
      .then(response => response.json())
      .then(data => setStore('todos', data.slice(0, 10)))
  })

  const addTodo = (title) => {
    if (!title) return;

    setStore('todos', (todos) => [...todos, { id: Date.now(), title, completed: false }]);
  }

  const toggleTodo = (id) => {
    setStore('todos', (t) => t.id === id, 'completed', (completed) => !completed);
  };

  const deleteTodo = (id) => {
    setStore('todos', (t) => t.filter((t) => t.id !== id))
  }

  const length = createMemo(() => store.todos.length)

  return (
    <div class="max-w-sm md:max-w-lg mx-auto my-10 bg-white rounded-md shadow-md overflow-hidden">
      <h1 class="text-2xl font-bold text-center py-4 bg-gray-100">{props.title}</h1>
      {length() > 0 && (
        <ul class="list-none p-4">
          <For each={store.todos}>
            {todo => <TodoItem key={todo.id} todo={todo} onToggle={toggleTodo} onRemove={deleteTodo} />}
          </For>
        </ul>
      )}
      <TodoForm onSubmit={addTodo} />
    </div>
  )
}

export default TodoList
